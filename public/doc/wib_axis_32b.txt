
+---------------------------------------------------------------+
| WIB1 axis 32b (quick summary)                                 |
+---------------------------------------------------------------+
|        data       v u l k   v=tvalid u=tuser l=tlast k=tkeep  |
|        0x87654321 1 0 0 1   header or data                    |
|        ...                  header or data                    |
|        0x00554a00 1 0 1 1   EOP (end-of-packet)               |
+---------------------------------------------------------------+


      1 0x00554a00 1 0 0 1     # wib header                    0x0(8b), version(5b), fiber(3b), crate(5b), slot(3b), reserved(8b)
      2 0x55550003 1 0 0 1     # wib header      -> 5 words    oosmm(2b), reserved(14b), wiberrors(16b)
      3 0x55555555 1 0 0 1     # wib header                    timestamp(32b) LSBs
      4 0x55555555 1 0 0 1     # wib header                    timestamp(32b) MSBs
      5 0xaa550025 1 0 0 1     # coldata 1 header                    err1(4b), err2(4b), reserved(8b), chksmA(8b) LSB, chksmB(8b) LSB           
      6 0x5555aa55 1 0 0 1     # coldata 1 header      -> 4 words    chksmA(8b) MSB, chksmB(8b) MSB, colcount(16b) 
      7 0x00005555 1 0 0 1     # coldata 1 header                    errorreg(16b), reserved(16b)
      8 0x87654321 1 0 0 1     # coldata 1 header                    hdr1(4b), hdr2(4b), hdr3(4b), hdr4(4b), hdr5(4b), hdr6(4b), hdr7(4b), hdr8(4b),
      9 0x4141f4f4 1 0 0 1     # coldata 1 data word 1
     10 0xf4f41f1f 1 0 0 1      
     11 0x1f1f4141 1 0 0 1
     12 0x4141f4f4 1 0 0 1
     13 0xf4f41f1f 1 0 0 1
     14 0x1f1f4141 1 0 0 1
     15 0x4141f4f4 1 0 0 1 
     16 0xf4f41f1f 1 0 0 1
     17 0x1f1f4141 1 0 0 1
     18 0x4141f4f4 1 0 0 1
     19 0xf4f41f1f 1 0 0 1      # wib data format to store values from 8 ADCs with 8 channels per ADC
     20 0x1f1f4141 1 0 0 1      # https://docs.google.com/spreadsheets/d/1jV7h7imTM9oyPG1U075IKS6_oMQtsEcE26zcxrs0-tM/edit#gid=0
     21 0x4141f4f4 1 0 0 1
     22 0xf4f41f1f 1 0 0 1
     23 0x1f1f4141 1 0 0 1
     24 0x4141f4f4 1 0 0 1
     25 0xf4f41f1f 1 0 0 1
     26 0x1f1f4141 1 0 0 1
     27 0x4141f4f4 1 0 0 1
     28 0xf4f41f1f 1 0 0 1
     29 0x1f1f4141 1 0 0 1
     30 0x4141f4f4 1 0 0 1
     31 0xf4f41f1f 1 0 0 1
     32 0x1f1f4141 1 0 0 1      # coldata 1 data word 24
     33 0xaa550025 1 0 0 1      # coldata 2 header
     34 0x5555aa55 1 0 0 1      # coldata 2 header
     35 0x00005555 1 0 0 1      # coldata 2 header
     36 0x87654321 1 0 0 1      # coldata 2 header
     37 0x4141f4f4 1 0 0 1      # coldata 2 data word 1
     38 0xf4f41f1f 1 0 0 1
     39 0x1f1f4141 1 0 0 1
     40 0x4141f4f4 1 0 0 1
     41 0xf4f41f1f 1 0 0 1
     42 0x1f1f4141 1 0 0 1
     43 0x4141f4f4 1 0 0 1
     44 0xf4f41f1f 1 0 0 1
     45 0x1f1f4141 1 0 0 1
     46 0x4141f4f4 1 0 0 1
     47 0xf4f41f1f 1 0 0 1
     48 0x1f1f4141 1 0 0 1
     49 0x4141f4f4 1 0 0 1
     50 0xf4f41f1f 1 0 0 1
     51 0x1f1f4141 1 0 0 1
     52 0x4141f4f4 1 0 0 1
     53 0xf4f41f1f 1 0 0 1
     54 0x1f1f4141 1 0 0 1
     55 0x4141f4f4 1 0 0 1
     56 0xf4f41f1f 1 0 0 1
     57 0x1f1f4141 1 0 0 1
     58 0x4141f4f4 1 0 0 1
     59 0xf4f41f1f 1 0 0 1
     60 0x1f1f4141 1 0 0 1      # coldata 2 data word 24
     61 0xaa550025 1 0 0 1      # coldata 3 header
     62 0x5555aa55 1 0 0 1      # coldata 3 header
     63 0x00005555 1 0 0 1      # coldata 3 header
     64 0x87654321 1 0 0 1      # coldata 3 header
     65 0x4141f4f4 1 0 0 1      # coldata 3 data word 1
     66 0xf4f41f1f 1 0 0 1
     67 0x1f1f4141 1 0 0 1
     68 0x4141f4f4 1 0 0 1
     69 0xf4f41f1f 1 0 0 1
     70 0x1f1f4141 1 0 0 1
     71 0x4141f4f4 1 0 0 1
     72 0xf4f41f1f 1 0 0 1
     73 0x1f1f4141 1 0 0 1
     74 0x4141f4f4 1 0 0 1
     75 0xf4f41f1f 1 0 0 1
     76 0x1f1f4141 1 0 0 1
     77 0x4141f4f4 1 0 0 1
     78 0xf4f41f1f 1 0 0 1
     79 0x1f1f4141 1 0 0 1
     80 0x4141f4f4 1 0 0 1
     81 0xf4f41f1f 1 0 0 1
     82 0x1f1f4141 1 0 0 1
     83 0x4141f4f4 1 0 0 1
     84 0xf4f41f1f 1 0 0 1
     85 0x1f1f4141 1 0 0 1
     86 0x4141f4f4 1 0 0 1
     87 0xf4f41f1f 1 0 0 1
     88 0x1f1f4141 1 0 0 1      # coldata 3 data word 24
     89 0xaa550025 1 0 0 1      # coldata 4 header
     90 0x5555aa55 1 0 0 1      # coldata 4 header
     91 0x00005555 1 0 0 1      # coldata 4 header
     92 0x87654321 1 0 0 1      # coldata 4 header
     93 0x4141f4f4 1 0 0 1      # coldata 4 data word 1
     94 0xf4f41f1f 1 0 0 1
     95 0x1f1f4141 1 0 0 1
     96 0x4141f4f4 1 0 0 1
     97 0xf4f41f1f 1 0 0 1
     98 0x1f1f4141 1 0 0 1
     99 0x4141f4f4 1 0 0 1
    100 0xf4f41f1f 1 0 0 1
    101 0x1f1f4141 1 0 0 1
    102 0x4141f4f4 1 0 0 1
    103 0xf4f41f1f 1 0 0 1
    104 0x1f1f4141 1 0 0 1
    105 0x4141f4f4 1 0 0 1
    106 0xf4f41f1f 1 0 0 1
    107 0x1f1f4141 1 0 0 1
    108 0x4141f4f4 1 0 0 1
    109 0xf4f41f1f 1 0 0 1
    110 0x1f1f4141 1 0 0 1
    111 0x4141f4f4 1 0 0 1
    112 0xf4f41f1f 1 0 0 1
    113 0x1f1f4141 1 0 0 1
    114 0x4141f4f4 1 0 0 1
    115 0xf4f41f1f 1 0 0 1
    116 0x1f1f4141 1 0 1 1      # coldata 4 data word 24
    117 0x00554a00 1 0 0 1      # start of next wib 32b axis packet
    118 0x55550003 1 0 0 1
    119 0x5555556e 1 0 0 1
    120 0x55555555 1 0 0 1
    121 0xaa550025 1 0 0 1
    122 0x5555aa55 1 0 0 1
    123 0x00005555 1 0 0 1
    124 0x87654321 1 0 0 1    
    ...                         # 7424 lines for 64 wib 33b packets      -> 116 x 64 = 7680
