      1 0x5500 1 0 0 1      # axi4s header word 1  crate(10b), fibre/linkid(6b)      -> start of first axi4s unpacker packet (e.g. pipeline 0)
      2 0x4000 1 0 0 1      # axi4s header word 2  wire/channel number(8b), slot(4b), flags(4b)
      3 0x5555 1 0 0 1      # axi4s header word 3  time stamp LSB
      4 0x5555 1 0 0 1      # axi4s header word 4  time stamp
      5 0x5555 1 0 0 1      # axi4s header word 5  time stamp
      6 0x5555 1 1 0 1      # axi4s header word 6  time stamp MSB
      7 0x01f4 1 0 0 1      # adc/hit data word 1
      8 0x01f4 1 0 0 1      
      9 0x01f4 1 0 0 1
     10 0x01f4 1 0 0 1
     11 0x01f4 1 0 0 1
     12 0x01f4 1 0 0 1
     13 0x01f4 1 0 0 1
     14 0x01f4 1 0 0 1
     15 0x01f4 1 0 0 1
     16 0x01f4 1 0 0 1
     17 0x01f4 1 0 0 1
     18 0x01f4 1 0 0 1
     19 0x01f4 1 0 0 1
     20 0x01f4 1 0 0 1
     21 0x01f4 1 0 0 1
     22 0x01f4 1 0 0 1
     23 0x01f4 1 0 0 1
     24 0x01f4 1 0 0 1
     25 0x01f4 1 0 0 1
     26 0x01f5 1 0 0 1
     27 0x01f6 1 0 0 1
     28 0x01f8 1 0 0 1
     29 0x01fa 1 0 0 1
     30 0x01fd 1 0 0 1
     31 0x0201 1 0 0 1
     32 0x0206 1 0 0 1
     33 0x020c 1 0 0 1
     34 0x0212 1 0 0 1
     35 0x0218 1 0 0 1
     36 0x021d 1 0 0 1
     37 0x0222 1 0 0 1
     38 0x0225 1 0 0 1
     39 0x0226 1 0 0 1
     40 0x0225 1 0 0 1
     41 0x0222 1 0 0 1
     42 0x021d 1 0 0 1
     43 0x0218 1 0 0 1
     44 0x0212 1 0 0 1
     45 0x020c 1 0 0 1
     46 0x0206 1 0 0 1
     47 0x0201 1 0 0 1
     48 0x01fd 1 0 0 1
     49 0x01fa 1 0 0 1
     50 0x01f8 1 0 0 1
     51 0x01f6 1 0 0 1
     52 0x01f5 1 0 0 1
     53 0x01f4 1 0 0 1
     54 0x01f4 1 0 0 1
     55 0x01f4 1 0 0 1
     56 0x01f4 1 0 0 1
     57 0x01f4 1 0 0 1
     58 0x01f4 1 0 0 1
     59 0x01f4 1 0 0 1
     60 0x01f4 1 0 0 1
     61 0x01f4 1 0 0 1
     62 0x01f4 1 0 0 1
     63 0x01f4 1 0 0 1
     64 0x01f4 1 0 0 1
     65 0x01f4 1 0 0 1
     66 0x01f4 1 0 0 1
     67 0x01f4 1 0 0 1
     68 0x01f4 1 0 0 1
     69 0x01f4 1 0 0 1
     70 0x01f4 1 1 1 1      # adc/hit data word 64      -> end of first axi4s unpacker packet (e.g. pipeline 0)      -> 70 lines per axi4s 16b packet
     71 0x5504 1 0 0 1      # start of next axi4s unpacker packet
     72 0x4000 1 0 0 1
     73 0x5555 1 0 0 1
     74 0x5555 1 0 0 1
     75 0x5555 1 0 0 1
     76 0x5555 1 1 0 1
     77 0x01f4 1 0 0 1
     78 0x01f4 1 0 0 1
     ...                    # 4480 lines for 64 (ticks) 16b axi4s packets      -> 70 x 64 = 4480
