# https://cernbox.cern.ch/index.php/s/Oxh36ZcA7ZgnL0Y                                               (WIB v3)
# https://drive.google.com/file/d/1GagXB0OHX075KkE9jvGqEusbbhGIvlwq/view                            (WIB v2)
# https://docs.google.com/spreadsheets/d/1jV7h7imTM9oyPG1U075IKS6_oMQtsEcE26zcxrs0-tM/edit#gid=0    (WIB v1)

---START OF FILE
01  0,0,0,0,0,0,0,0          # version(6b), detid(6b), crate(10b), slot(4b), linkid(6b), timestamp(64b),       (3 words)
                             # tbd13(13b), cdtsid(3b), fembvalid(2b), fembmask(8b), lol(1b), tbd5(5b),         (1 word) 
                             # fembpif(8b), fembsync(8b), coldatats(15b), tbd1(1b)   WIB header (5 words)      (1 word) 
02  0,0,0,0,0,0,0,0          # FEMB 0 U wire  ADC 1-8
03  0,0,0,0,0,0,0,0          # FEMB 0 U wire  ADC 9-16
04  0,0,0,0,0,0,0,0          # FEMB 0 U wire  ADC 17-24
05  0,0,0,0,0,0,0,0          # FEMB 0 U wire  ADC 25-32
06  0,0,0,0,0,0,0,0          # FEMB 0 U wire  ADC 33-40
07  0,0,0,0,0,0,0,0          # FEMB 0 V wire  ADC 1-8
08  0,0,0,0,0,0,0,0          # FEMB 0 V wire  ADC 9-16
09  0,0,0,0,0,0,0,0          # FEMB 0 V wire  ADC 17-24
10  0,0,0,0,0,0,0,0          # FEMB 0 V wire  ADC 25-32
11  0,0,0,0,0,0,0,0          # FEMB 0 V wire  ADC 33-40
12  0,0,0,0,0,0,0,0          # FEMB 0 X wire  ADC 1-8
13  0,0,0,0,0,0,0,0          # FEMB 0 X wire  ADC 9-16
14  0,0,0,0,0,0,0,0          # FEMB 0 X wire  ADC 17-24
15  0,0,0,0,0,0,0,0          # FEMB 0 X wire  ADC 25-32
16  0,0,0,0,0,0,0,0          # FEMB 0 X wire  ADC 33-40
17  0,0,0,0,0,0,0,0          # FEMB 0 X wire  ADC 41-48
18  0,0,0,0,0,0,0,0          # FEMB 1 U wire  ADC 1-8
19  0,0,0,0,0,0,0,0          # FEMB 1 U wire  ADC 9-16
20  0,0,0,0,0,0,0,0          # FEMB 1 U wire  ADC 17-24
21  0,0,0,0,0,0,0,0          # FEMB 1 U wire  ADC 25-32
22  0,0,0,0,0,0,0,0          # FEMB 1 U wire  ADC 33-40
23  0,0,0,0,0,0,0,0          # FEMB 1 V wire  ADC 1-8
24  0,0,0,0,0,0,0,0          # FEMB 1 V wire  ADC 9-16
25  0,0,0,0,0,0,0,0          # FEMB 1 V wire  ADC 17-24
26  0,0,0,0,0,0,0,0          # FEMB 1 V wire  ADC 25-32
27  0,0,0,0,0,0,0,0          # FEMB 1 V wire  ADC 33-40
28  0,0,0,0,0,0,0,0          # FEMB 1 X wire  ADC 1-8
29  0,0,0,0,0,0,0,0          # FEMB 1 X wire  ADC 9-16
30  0,0,0,0,0,0,0,0          # FEMB 1 X wire  ADC 17-24
31  0,0,0,0,0,0,0,0          # FEMB 1 X wire  ADC 25-32
32  0,0,0,0,0,0,0,0          # FEMB 1 X wire  ADC 33-40
33  0,0,0,0,0,0,0,0          # FEMB 1 X wire  ADC 41-48
34  0,0,0,0,0,0,0            # flex(16b), tbd1(1b), tbd2(1b), ws(1b), psrcal(4b), ready(1b), context(8b)  WIB footer (1 word) 
35                           # empty line      -> 34 lines per wib packet 
36  0,0,0,0,0,0,0,0,...      # WIB header (5 words)
37  0,0,0,0,0,0,0,0          # FEMB 0 U wire  ADC 1-8
38  ...
39  ...

---END OF FILE

