      1 0x0000003c 1      # wib header                    SOF word, start of frame is K.28.1 (0x3c)    -> this header word is removed by FELIX  
      2 0x00554a00 0      # wib header                    0x0(8b), version(5b), fiber(3b), crate(5b), slot(3b), reserved(8b)
      3 0x55550003 0      # wib header      -> 5 words    oosmm(2b), reserved(14b), wiberrors(16b)
      4 0x55555555 0      # wib header                    timestamp(32b) LSBs
      5 0x55555555 0      # wib header                    timestamp(32b) MSBs
      6 0xaa550025 0      # coldata 1 header                    err1(4b), err2(4b), reserved(8b), chksmA(8b) LSB, chksmB(8b) LSB           
      7 0x5555aa55 0      # coldata 1 header      -> 4 words    chksmA(8b) MSB, chksmB(8b) MSB, colcount(16b) 
      8 0x00005555 0      # coldata 1 header                    errorreg(16b), reserved(16b)
      9 0x87654321 0      # coldata 1 header                    hdr1(4b), hdr2(4b), hdr3(4b), hdr4(4b), hdr5(4b), hdr6(4b), hdr7(4b), hdr8(4b),
     10 0x4141f4f4 0      # coldata 1 data word 1
     11 0xf4f41f1f 0      
     12 0x1f1f4141 0
     13 0x4141f4f4 0
     14 0xf4f41f1f 0
     15 0x1f1f4141 0
     16 0x4141f4f4 0 
     17 0xf4f41f1f 0
     18 0x1f1f4141 0
     19 0x4141f4f4 0
     20 0xf4f41f1f 0      # wib data format to store values from 8 ADCs with 8 channels per ADC
     21 0x1f1f4141 0      # https://docs.google.com/spreadsheets/d/1jV7h7imTM9oyPG1U075IKS6_oMQtsEcE26zcxrs0-tM/edit#gid=0
     22 0x4141f4f4 0
     23 0xf4f41f1f 0
     24 0x1f1f4141 0
     25 0x4141f4f4 0
     26 0xf4f41f1f 0
     27 0x1f1f4141 0
     28 0x4141f4f4 0
     29 0xf4f41f1f 0
     30 0x1f1f4141 0
     31 0x4141f4f4 0
     32 0xf4f41f1f 0
     33 0x1f1f4141 0      # coldata 1 data word 24
     34 0xaa550025 0      # coldata 2 header
     35 0x5555aa55 0      # coldata 2 header
     36 0x00005555 0      # coldata 2 header
     37 0x87654321 0      # coldata 2 header
     38 0x4141f4f4 0      # coldata 2 data word 1
     39 0xf4f41f1f 0
     40 0x1f1f4141 0
     41 0x4141f4f4 0
     42 0xf4f41f1f 0
     43 0x1f1f4141 0
     44 0x4141f4f4 0
     45 0xf4f41f1f 0
     46 0x1f1f4141 0
     47 0x4141f4f4 0
     48 0xf4f41f1f 0
     49 0x1f1f4141 0
     50 0x4141f4f4 0
     51 0xf4f41f1f 0
     52 0x1f1f4141 0
     53 0x4141f4f4 0
     54 0xf4f41f1f 0
     55 0x1f1f4141 0
     56 0x4141f4f4 0
     57 0xf4f41f1f 0
     58 0x1f1f4141 0
     59 0x4141f4f4 0
     60 0xf4f41f1f 0
     61 0x1f1f4141 0      # coldata 2 data word 24
     62 0xaa550025 0      # coldata 3 header
     63 0x5555aa55 0      # coldata 3 header
     64 0x00005555 0      # coldata 3 header
     65 0x87654321 0      # coldata 3 header
     66 0x4141f4f4 0      # coldata 3 data word 1
     67 0xf4f41f1f 0
     68 0x1f1f4141 0
     69 0x4141f4f4 0
     70 0xf4f41f1f 0
     71 0x1f1f4141 0
     72 0x4141f4f4 0
     73 0xf4f41f1f 0
     74 0x1f1f4141 0
     75 0x4141f4f4 0
     76 0xf4f41f1f 0
     77 0x1f1f4141 0
     78 0x4141f4f4 0
     79 0xf4f41f1f 0
     80 0x1f1f4141 0
     81 0x4141f4f4 0
     82 0xf4f41f1f 0
     83 0x1f1f4141 0
     84 0x4141f4f4 0
     85 0xf4f41f1f 0
     86 0x1f1f4141 0
     87 0x4141f4f4 0
     88 0xf4f41f1f 0
     89 0x1f1f4141 0      # coldata 3 data word 24
     90 0xaa550025 0      # coldata 4 header
     91 0x5555aa55 0      # coldata 4 header
     92 0x00005555 0      # coldata 4 header
     93 0x87654321 0      # coldata 4 header
     94 0x4141f4f4 0      # coldata 4 data word 1
     95 0xf4f41f1f 0
     96 0x1f1f4141 0
     97 0x4141f4f4 0
     98 0xf4f41f1f 0
     99 0x1f1f4141 0
    100 0x4141f4f4 0
    101 0xf4f41f1f 0
    102 0x1f1f4141 0
    103 0x4141f4f4 0
    104 0xf4f41f1f 0
    105 0x1f1f4141 0
    106 0x4141f4f4 0
    107 0xf4f41f1f 0
    108 0x1f1f4141 0
    109 0x4141f4f4 0
    110 0xf4f41f1f 0
    111 0x1f1f4141 0
    112 0x4141f4f4 0
    113 0xf4f41f1f 0
    114 0x1f1f4141 0
    115 0x4141f4f4 0
    116 0xf4f41f1f 0
    117 0x1f1f4141 0      # coldata 4 data word 24
    118 0x000000dc 1      # EOF end-of-frame word 1
    119 0x000000bc 1      # EOF end-of-frame word 2
    120 0x000000bc 1      # EOF end-of-frame word 3      -> end of 1st wib 33b packet      -> 120 lines per wib 33b packet 
    121 0x0000003c 1      # start of next wib 33b packet 
    122 0x00554a00 0
    123 0x55550003 0
    124 0x5555556e 0
    125 0x55555555 0
    126 0xaa550025 0
    127 0x5555aa55 0
    128 0x00005555 0
    129 0x87654321 0
    ...                   # 7680 lines for 64 wib 33b packets      -> 120 x 64 = 7680
