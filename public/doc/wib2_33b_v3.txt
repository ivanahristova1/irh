---START OF FILE
      1 0x0000003c 1    # wib2 header                    SOF word, start of frame is K.28.1 (0x3c)    -> this header word is removed by FELIX 
      2 0x00000000 0    # wib2 header                    version(6b), detid(6b), crate(10b), slot(4b), linkid(6b)
      3 0x00000000 0    # wib2 header      -> 5 words    timestamp(32b) LSBs
      4 0x49de207d 0    # wib2 header                    timestamp(32b) MSBs                   
      5 0x641d0f18 0    # wib2 header                    word4 various bitfields                   
      6 0x00000000 0    # wib2 header                    word5 various bitfields
      6 0x00000000 0    # wib2 data word 1  FEMB 0 U wire  ADC1(14b), ADC2(14b), ADC3(4b) LSB
      7 0x00000000 0    # wib2 data word 2  FEMB 0 U wire  ADC3(10b) MSB, ADC4(14b), ADC5(8b) LSB
      8 0x00000000 0    # wib2 data word 3  ...
      9 0x00000000 0    
     10 0x00000000 0
     11 0x00000000 0
     12 0x00000000 0
     13 0x00000000 0
     14 0x00000000 0
     15 0x00000000 0
     16 0x00000000 0
     17 0x00000000 0
     18 0x00000000 0
     19 0x00000000 0
     20 0x00000000 0
     21 0x00000000 0
     22 0x00000000 0
     23 0x00000000 0  # wib2 data word 18  FEMB 0 U/V wire  ADC39(2b) MSB, ADC40(14b), ADC1(14b), ADC2(2b) LSB 
     24 0x00000000 0  # wib2 data word 19  FEMB 0 V wire    ADC2(12b) MSB, ADC3(14b), ADC4(6b) LSB
     25 0x00000000 0  # wib2 data word 20  ...
     26 0x00000000 0
     27 0x00000000 0
     28 0x00000000 0
     29 0x00000000 0
     30 0x00000000 0
     31 0x00000000 0
     32 0x00000000 0
     33 0x00000000 0
     34 0x00000000 0
     35 0x00000000 0
     36 0x00000000 0
     37 0x00000000 0
     38 0x00000000 0
     39 0x00000000 0
     40 0x00000000 0  # wib2 data word 35  FEMB 0 X wire  ADC1(14b), ADC2(14b), ADC3(4b) LSB
     41 0x00000000 0  # wib2 data word 36  FEMB 0 X wire  ADC3(10b) MSB, ADC4(14b), ADC5(8b) LSB
     42 0x00000000 0  # wib2 data word 37  ...
     43 0x00000000 0
     44 0x00000000 0
     45 0x00000000 0
     46 0x00000000 0
     47 0x00000000 0
     48 0x00000000 0
     49 0x00000000 0
     50 0x00000000 0
     51 0x00000000 0
     52 0x00000000 0
     53 0x00000000 0
     54 0x00034ad0 0
     55 0x00000000 0
     56 0x00000000 0
     57 0x00000000 0
     58 0x00000000 0
     59 0x00000000 0
     60 0x00000000 0
     61 0x00000000 0  # wib2 data word 56  FEMB 0 X wire  ADC46(4b) MSB, ADC47(14b), ADC48(14b)
     62 0x00000000 0  # wib2 data word 57  FEMB 1 U wire 
     63 0x00000000 0
     64 0x00000000 0
     65 0x00000000 0
     66 0x00000000 0
     67 0x00000000 0
     68 0x00000000 0
     69 0x00000000 0
     70 0x00000000 0
     71 0x00000000 0
     72 0x00000000 0
     73 0x00000000 0
     74 0x00000000 0
     75 0x00000000 0
     76 0x00000000 0
     77 0x00000000 0
     78 0x00000000 0
     79 0x00000000 0  # wib2 data word 74  FEMB 1 U/V wire
     80 0x00000000 0
     81 0x00000000 0
     82 0x00000000 0
     83 0x00000000 0
     84 0x00000000 0
     85 0x00000000 0
     86 0x00000000 0
     87 0x00000000 0
     88 0x00000000 0
     89 0x00000000 0
     90 0x00000000 0
     91 0x00000000 0
     92 0x00000000 0
     93 0x00000000 0
     94 0x00000000 0
     95 0x00000000 0
     96 0x00000000 0  # wib2 data word 91  FEMB 1 X wire
     97 0x00000000 0
     98 0x00000000 0
     99 0x00000000 0
    100 0x00000000 0
    101 0x00000000 0
    102 0x00000000 0
    103 0x00000000 0
    104 0x00000000 0
    105 0x00000000 0
    106 0x00000000 0
    107 0x00000000 0
    108 0x00000000 0
    109 0x00000000 0
    110 0x00000000 0
    111 0x00000000 0
    112 0x00000000 0
    113 0x00000000 0
    114 0x00000000 0
    115 0x00000000 0
    116 0x00000000 0
    117 0x00000000 0  # wib2 data word 112  FEMB 1 X wire
    118 0x000000dc 1  # EOF end-of-frame word 1
    119 0x000000bc 1  # EOF end-of-frame word 2
    120 0x000000bc 1  # EOF end-of-frame word 3      -> end of 1st wib 33b packet      -> 120 lines per wib2 33b packet
    121 0x0000003c 1  # start of next wib 33b packet
    122 0x00000000 0
    123 0x00000000 0
    124 0x93f28ccf 0
    125 0x623f1be6 0
    126 0x00000000 0
    127 0x00000000 0
    128 0x00000000 0
    129 0x00000000 0
    130 0x00000000 0
    131 0x00000000 0
    132 0x00000000 0
    133 0x00000000 0
    134 0x00000000 0
    ... 
    ...               # 7680 lines for 64 wibb2 33b packets      -> 120 x 64 = 7680
---END OF FILE
